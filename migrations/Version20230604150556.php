<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230604150556 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE board_post DROP FOREIGN KEY board_post_user_id_fk');
        $this->addSql('DROP INDEX board_post_user_id_fk ON board_post');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE board_post ADD CONSTRAINT board_post_user_id_fk FOREIGN KEY (created_by) REFERENCES user (id)');
        $this->addSql('CREATE INDEX board_post_user_id_fk ON board_post (created_by)');
    }
}
