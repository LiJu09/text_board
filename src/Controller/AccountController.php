<?php

namespace App\Controller;

use App\Entity\BoardPost;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class AccountController extends AbstractController
{

    #[Route('/account', name: 'app_account')]
    public function index(ManagerRegistry $doctrine): Response
    {
        if (!$this->isGranted('IS_AUTHENTICATED_FULLY')) {
            return $this->redirectToRoute('app_login');
        }

        $user = "";
        if ($this->getUser() != null){$user = $this->getUser();}

        $repository = $doctrine->getRepository(BoardPost::class);

        $posts = $repository->findBy(
            [
                'state' => 'ok',
                'created_by' => $this->getUser()->getId()
            ],
            ['id' => 'DESC']);

        return $this->render('account/index.html.twig', [
            'user' => $user,
            'posts' => $posts,
        ]);
    }

    #[Route('/account/delete', name: 'app_account_delete')]
    public function delete(ManagerRegistry $doctrine, SessionInterface $session, TokenStorageInterface $tokenStorage): Response
    {
        if (!$this->isGranted('IS_AUTHENTICATED_FULLY')) {
            return $this->redirectToRoute('app_login');
        }

        $user = $this->getUser();

        $entityManager = $doctrine->getManager();

        if (!$user) {
            return $this->redirectToRoute('app_board');
        }

        $entityManager->remove($user);

        $entityManager->flush();

        $tokenStorage->setToken(null);
        $session->invalidate();

        return $this->redirectToRoute('app_board');
    }
}
