<?php

namespace App\Controller;

use App\Entity\User;
use Doctrine\Persistence\ManagerRegistry;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminController extends AbstractController
{
    #[IsGranted('ROLE_ADMIN')]
    #[Route('/admin', name: 'app_admin')]
    public function index(ManagerRegistry $doctrine): Response
    {
        $repository = $doctrine->getRepository(User::class);

        $users = $repository->findAll();

        return $this->render('admin/index.html.twig', [
            'users' => $users,
        ]);
    }

    #[IsGranted('ROLE_ADMIN')]
    #[Route('/admin/user/{id<\d+>}', name: 'app_admin_user')]
    public function user(ManagerRegistry $doctrine, int $id): Response
    {
        $user = $doctrine->getRepository(User::class)->find($id);

        if (!$user) {
            return $this->redirectToRoute('app_admin');
        }

        return $this->render('admin/user.html.twig', [
            'user' => $user
        ]);
    }

    #[IsGranted('ROLE_SUPER_ADMIN')]
    #[Route('/admin/user/{id<\d+>}/promote', name: 'app_admin_user_promote')]
    public function promote(ManagerRegistry $doctrine, int $id): Response
    {
        $entityManager = $doctrine->getManager();
        $user = $doctrine->getRepository(User::class)->find($id);

        if (!$user) {
            return $this->redirectToRoute('app_admin');
        }

        $user->addRoles(['ROLE_ADMIN']);

        $entityManager->flush();

        return $this->render('admin/user.html.twig', [
            'user' => $user
        ]);
    }

    #[IsGranted('ROLE_SUPER_ADMIN')]
    #[Route('/admin/user/{id<\d+>}/demote', name: 'app_admin_user_demote')]
    public function demote(ManagerRegistry $doctrine, int $id): Response
    {
        $entityManager = $doctrine->getManager();
        $user = $doctrine->getRepository(User::class)->find($id);

        if (!$user) {
            return $this->redirectToRoute('app_admin');
        }

        if($this->getUser() == $user) {
            return new Response("You cant demote your account");
        }

        $user->removeRoles(['ROLE_ADMIN']);

        $entityManager->flush();

        return $this->render('admin/user.html.twig', [
            'user' => $user
        ]);
    }

    #[IsGranted('ROLE_SUPER_ADMIN')]
    #[Route('/admin/user/{id<\d+>}/delete', name: 'app_admin_user_delete')]
    public function delete(ManagerRegistry $doctrine, int $id): Response
    {
        $entityManager = $doctrine->getManager();
        $user = $doctrine->getRepository(User::class)->find($id);

        if (!$user) {
            return $this->redirectToRoute('app_admin');
        }

        if($this->getUser() == $user) {
            return new Response("You cant delete your account");
        }

        $entityManager->remove($user);

        $entityManager->flush();

        return $this->redirectToRoute('app_admin');
    }
}
