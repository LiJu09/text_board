<?php

namespace App\Controller;

use App\Entity\BoardPost;
use App\Entity\User;
use App\Form\BoardPostType;
use Doctrine\Persistence\ManagerRegistry;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class BoardPostController extends AbstractController
{
    #[Route('/', name: 'app_board')]
    public function index(ManagerRegistry $doctrine): Response
    {
        $repository = $doctrine->getRepository(BoardPost::class);

        $roles = [];
        if ($this->getUser() != null){$roles = $this->getUser()->getRoles();}

        if (in_array('ROLE_ADMIN', $roles)){
            $posts = $repository->findBy(
                [],
                [
                    'state' => 'DESC',
                    'id' => 'DESC'
                ]
            );
        } else {
            $posts = $repository->findBy(
                ['state' => 'ok'],
                ['id' => 'DESC']);
        }

        return $this->render('board_post/index.html.twig', [
            'posts' => $posts,
        ]);
    }

    #[Route('/post/{id<\d+>}', name: 'app_board_post_show')]
    public function show(ManagerRegistry $doctrine, int $id): Response
    {
        $post = $doctrine->getRepository(BoardPost::class)->find($id);

        if (!$post) {
            return $this->redirectToRoute('app_board');
        }

        $user = $doctrine->getRepository(User::class)->findOneBy(['id' => $post->getCreatedBy()]);

        if ($post->getState() == 'ok') {
            return $this->render('board_post/post.html.twig', [
                'post' => $post,
                'user' => $user,
            ]);
        } else {
            $roles = [];
            if ($this->getUser() != null){$roles = $this->getUser()->getRoles();}
            if (in_array('ROLE_ADMIN', $roles)){
                return $this->render('board_post/post.html.twig', [
                    'post' => $post,
                    'user' => $user,
                ]);
            } else {
                return $this->redirectToRoute('app_board');
            }
        }
    }

    #[Route('/create_post', name: 'app_board_create_post')]
    public function createPost(Request $request, ManagerRegistry $doctrine, ValidatorInterface $validator): Response
    {
        $post = new BoardPost();

        $form = $this->createForm(BoardPostType::class, $post);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $post = $form->getData();
            $post->setState('ok');
            if ($this->getUser() != null){$post->setCreatedBy($this->getUser()->getId());}
            else $post->setCreatedBy(0);

//            $roles = [];
//            if ($this->getUser() != null){$roles = $this->getUser()->getRoles();}
//            if (in_array('ROLE_ADMIN', $roles)){
//                $post->setState('ok');
//            } else {
//                $post->setState('unapproved');
//            }

            $entityManager = $doctrine->getManager();

            // tell Doctrine you want to (eventually) save the Product (no queries yet)
            $entityManager->persist($post);

            // actually executes the queries (i.e. the INSERT query)
            $entityManager->flush();

            //return new Response('Saved new post with id '.$post->getId());
//            $message = 'Post created successfully';
//            if ($post->getState() != 'ok'){
//                $message .= ', waiting for approval';
//            }
//            return $this->render('board_post/post.html.twig', [
//                'post' => $post,
//                'message' => $message
//            ]);
            return $this->redirectToRoute('app_board_post_show', ['id' => $post->getId()]);
        }

        return $this->renderForm('board_post/new.html.twig', [
            'form' => $form,
        ]);
    }

    #[IsGranted('ROLE_ADMIN')]
    #[Route('/post/{id<\d+>}/approve', name: 'app_board_approve_post')]
    public function approvePost(ManagerRegistry $doctrine, int $id): Response
    {
        $entityManager = $doctrine->getManager();
        $post = $doctrine->getRepository(BoardPost::class)->find($id);

        if (!$post) {
            throw $this->createNotFoundException(
                'No post found for id ' . $id
            );
        }

        $post->setState('ok');

        $entityManager->flush();

        return $this->redirectToRoute('app_board_post_show', ['id' => $id]);
    }

    #[IsGranted('ROLE_ADMIN')]
    #[Route('/post/{id<\d+>}/delete', name: 'app_board_delete_post')]
    public function deletePost(ManagerRegistry $doctrine, int $id): Response
    {
        $post = $doctrine->getRepository(BoardPost::class)->find($id);

        if (!$post) {
            throw $this->createNotFoundException(
                'No post found for id ' . $id
            );
        }

        $entityManager = $doctrine->getManager();
        $post->setState('deleted');
        $entityManager->flush();

        return $this->redirectToRoute('app_board_post_show', ['id' => $id]);
    }

    #[IsGranted('ROLE_SUPER_ADMIN')]
    #[Route('/post/{id<\d+>}/deletefromdb', name: 'app_board_delete_from_db_post')]
    public function deleteFromDBPost(ManagerRegistry $doctrine, int $id): Response
    {
        $post = $doctrine->getRepository(BoardPost::class)->find($id);

        if (!$post) {
            throw $this->createNotFoundException(
                'No post found for id ' . $id
            );
        }

        $entityManager = $doctrine->getManager();
        $entityManager->remove($post);
        $entityManager->flush();

        return $this->redirectToRoute('app_board');
    }
}
