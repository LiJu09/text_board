<?php

namespace App\Controller;

use App\Entity\BoardPost;
use App\Entity\User;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends AbstractController
{
    #[Route('/user/{id<\d+>}', name: 'app_user')]
    public function index(ManagerRegistry $doctrine, int $id): Response
    {
        $repository = $doctrine->getRepository(BoardPost::class);

        $posts = $repository->findBy(
            [
                'state' => 'ok',
                'created_by' => $id
            ],
            ['id' => 'DESC']);

        $repository = $doctrine->getRepository(User::class);

        $user = $repository->find($id);

        return $this->render('user/index.html.twig', [
            'controller_name' => 'UserController',
            'posts' => $posts,
            'user' => $user
        ]);
    }
}
