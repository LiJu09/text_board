<?php

namespace App\Entity;

use App\Repository\BoardPostRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: BoardPostRepository::class)]
class BoardPost
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 256)]
    private $title;

    #[Assert\Length(max: 2048, maxMessage: 'Max message length {{ limit }} characters')]
    #[ORM\Column(type: 'string', length: 2048, nullable: true)]
    private $content;

    #[ORM\Column(type: 'string', length: 256)]
    private $state;

    #[ORM\Column(type: 'integer')]
    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'id')]
    private $created_by;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(?string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getState(): ?string
    {
        return $this->state;
    }

    public function setState(string $state): self
    {
        $this->state = $state;

        return $this;
    }

    public function getCreatedBy(): ?int
    {
        return $this->created_by;
    }

    public function setCreatedBy(int $created_by): self
    {
        $this->created_by = $created_by;

        return $this;
    }
}
